/********* Tre3ceUtils.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>

@interface Tre3ceUtils : CDVPlugin {
  // Member variables go here.
}

- (void)isTimeSettingsAutomatic:(CDVInvokedUrlCommand*)command;
@end

@implementation Tre3ceUtils

- (void)isTimeSettingsAutomatic:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSString* echo = @"true";

    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:echo];

    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
