package com.nortesetenta.tre3ce;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import android.os.Build;
import android.provider.Settings;
import android.content.Context;

/**
 * This class echoes a string called from JavaScript.
 */
public class Tre3ceUtils extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("isTimeSettingsAutomatic")) {
            this.isTimeSettingsAutomatic(callbackContext);
            return true;
        }

        return false;
    }

    private void isTimeSettingsAutomatic(CallbackContext callbackContext){
        Log.d("Tre3ceUtils", "Lamada de funcion correcta!!");
        //callbackContext.error("Expected one non-empty string argument.");
        if (this.isTimeAutomatic(cordova.getActivity().getApplicationContext())){
            callbackContext.success("true");
        }else {
            callbackContext.success("false");
        }


    }

    private boolean isTimeAutomatic(Context c) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.Global.getInt(c.getContentResolver(), Settings.Global.AUTO_TIME, 0) == 1;
        } else {
            return android.provider.Settings.System.getInt(c.getContentResolver(), android.provider.Settings.System.AUTO_TIME, 0) == 1;
        }
    }
}
