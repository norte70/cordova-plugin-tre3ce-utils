var exec = require('cordova/exec');

module.exports = {
    isTimeSettingsAutomatic: function(success, error) {
        exec(success, error, "Tre3ceUtils", "isTimeSettingsAutomatic", []);
    }
}
